use std::{io::ErrorKind, path::Path};

use rocket::http::Status;
use rocket::{get, launch, routes};
use rocket_download_response::DownloadResponse;

#[get("/")]
async fn download() -> Result<DownloadResponse, Status> {
    let path = Path::join(Path::new("files"), "abomb.zip");

    DownloadResponse::from_file(path, None::<String>, None)
        .await
        .map_err(|err| {
            if err.kind() == ErrorKind::NotFound {
                Status::NotFound
            } else {
                Status::InternalServerError
            }
        })
}

#[get("/health")]
async fn health() -> Result<(), Status> {
    Ok(())
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![download, health])
}
