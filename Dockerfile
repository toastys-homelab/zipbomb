FROM rust:latest as builder
COPY . .
RUN cd webservd; cargo build --release
FROM ubuntu:jammy
COPY --from=builder webservd/target/release/webservd /webservd
COPY Rocket.toml /Rocket.toml
RUN mkdir /files
COPY abomb.zip /files/abomb.zip
WORKDIR /
ENTRYPOINT ["/webservd"]
